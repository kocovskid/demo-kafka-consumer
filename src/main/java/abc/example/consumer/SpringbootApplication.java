package abc.example.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.util.Properties;

@SpringBootApplication
public class SpringbootApplication {
    private static final Logger log = LoggerFactory.getLogger(SpringbootApplication.class);

    @Autowired
    Environment env;

    public static void main(String[] args) {
        // Add app name and version into MDC
        Properties prop = new Properties();
        try {
            //load a properties file from class path, inside static method
            prop.load(SpringbootApplication.class.getClassLoader().getResourceAsStream("META-INF/build-info.properties"));
            System.setProperty("app.name", prop.getProperty("build.name"));
            System.setProperty("app.version", prop.getProperty("build.version"));

        } catch (IOException ex) {
            log.warn("Failed to initialize loggging attributes: {}", ex.getMessage());
        }

        SpringApplication.run(SpringbootApplication.class, args);
    }


}
