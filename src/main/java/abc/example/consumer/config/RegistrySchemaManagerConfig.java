package abc.example.consumer.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Configuration
public class RegistrySchemaManagerConfig implements InitializingBean {
    private static final Logger log = LoggerFactory.getLogger(RegistrySchemaManagerConfig.class);


    @Value("${app.schema-registry.url}")
    String schemaRegistryUrl;

    @Value("${kafka.frt-topic-v1}")
    String topic1;

    @Value("${app.schema-registry.frt-topic-v1.schema.path}")
    String topic1Path;




    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Url={}", schemaRegistryUrl);
        RestClient restClient = RestClient.builder().baseUrl(schemaRegistryUrl).build();
        Map<String, String> topics = Map.of(topic1, topic1Path);
        // for each listener, get the topic name
        // locate the json schema file based on application properties
        // query the schema registry and find out what the current schema version is
        // If remote schema is same as app latest version
        //  - do nothing
        // ELSE
        // - Delete remote schema version if +1 > than app latest version (?)
        // - Add new schema if remote schema is -1 < than app latest version
        // - Else fail to run

        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        for (Map.Entry<String,String> topic : topics.entrySet()) {
            log.info("Examining [{}] for schema auto management...", topic.getKey());
            log.info("Sourcing from [{}]", topic.getValue());
            Resource[] schemaDir = resourcePatternResolver.getResources(String.format("classpath:" + topic.getValue()));
            if (schemaDir.length == 0) {
                throw new BeanInitializationException("Could not find registry schema resources");
            }
            if (Arrays.stream(schemaDir).filter(r -> r.isFile() && r.getFilename().endsWith(".json")).count() != 1) {
                throw new BeanInitializationException("One .json schema file required in registry schema resources");
            }
            Resource currentSchema = Arrays.stream(schemaDir).filter(r -> r.isFile() && r.getFilename().endsWith(".json")).findFirst().orElse(null);
            if (currentSchema == null) {
                throw new BeanInitializationException("No current .json schema file found in registry schema resources");
            }
            int currentVersion = Integer.parseInt(currentSchema.getFilename().replaceFirst("-.*", ""));
            Resource previousSchemaFiles = Arrays.stream(schemaDir)
                    .filter(r -> !r.isFile() && r.getFilename().equals("previous"))
                    .findFirst()
                    .orElse(null);
            Resource previousSchema = null;
            if (previousSchemaFiles != null) {
                List<Resource> sortedPreviousResources = Arrays.stream(
                        resourcePatternResolver.getResources(String.format("classpath:%s/*.json", previousSchemaFiles.getFile().getAbsolutePath())))
                        .sorted(Comparator.comparing(Resource::getFilename).reversed())
                        .toList();
                if (!CollectionUtils.isEmpty(sortedPreviousResources)) {
                    previousSchema = sortedPreviousResources.get(0);
                    // Do current and previous schema version checks to see if they are incremental
                    int previousVersion = Integer.parseInt(previousSchema.getFilename().replaceFirst("-.*", ""));
                    if (previousVersion + 1 != currentVersion) {
                        throw new BeanInitializationException(
                                String.format("Current version of schema is not incremental version of previous schema [current=%s, previous=%s]",
                                currentVersion, previousVersion));
                    }
                }
            } else {
                // Check that current schema starts at 1
                if (currentVersion != 1) {
                    throw new BeanInitializationException("Current version of schema needs to start with version 001");
                }
            }

            // query the schema registry and find out what the current schema version is
            int currentRemoteSchemaVersion = getCurrentRemoteSchemaVersion(restClient, topic.getKey());
            log.info("Latest Schema version: [{}]", currentSchema);
            log.info("Current remote schema version: [{}]", currentRemoteSchemaVersion);
            if (currentRemoteSchemaVersion != currentVersion) {
                // Rollback / Delete
                if (currentRemoteSchemaVersion == currentVersion + 1) {
                    log.info("Rolling back / deleting current remote schema version [{}]", currentRemoteSchemaVersion);
                    // Do this in bulk
                    // deleteSchema(restClient, sortedResources, latestVersion);
                } else if (currentRemoteSchemaVersion + 1 == currentVersion) {
                    log.info("Adding new schema version [{}]", currentVersion);
                    // Do this in bulk
                    // addSchemas(restClient, sortedResources, currentSchemaVersion, latestVersion);
                } else {
                    throw new BeanInitializationException(
                            String.format("Registry schemas are out of sync with their versions, aborting [current=%s, latest=%s]",
                            currentRemoteSchemaVersion, currentVersion)
                    );
                }
            }
        }
    }

//    GET /subjects/(string: subject)/versions
//
//    DELETE /subjects/(string: subject)/versions/(versionId: version)?permanent=true
//
//    POST /subjects/(string: subject)

    int getCurrentRemoteSchemaVersion(RestClient restClient, String topic) {
        Integer ret;
        try {
            List<Integer> versions = restClient
                    .get()
                    .uri(String.format("/subjects/%s/versions", topic))
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .body(List.class);
            log.info("versions={}", versions);
            ret = versions.get(0);
        } catch (HttpClientErrorException ex) {
            if (HttpStatus.NOT_FOUND.value() == ex.getStatusCode().value()) {
                // No schemas
                ret = 0;
            } else {
                throw ex;
            }
        }
        return ret;
    }

}
