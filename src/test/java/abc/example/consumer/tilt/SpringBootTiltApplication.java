package abc.example.consumer.tilt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

@Profile("tilt")
@SpringBootApplication
public class SpringBootTiltApplication {
  private static final Logger log = LoggerFactory.getLogger(SpringBootTiltApplication.class);
  private final Environment env;

  public SpringBootTiltApplication(Environment env) {
    this.env = env;
  }

  public static void main(String[] args) {
    SpringApplication.run(SpringBootTiltApplication.class, args);
  }
}
