## Demo-kafka-consumer

## Purpose
### Client for OpenApi + Json schema
Demonstrates the ability to use an openapi (or json schema file) as a maven dependency.
In this way a "client" application can generate its java pojos based on the api contract.

Using the dependency plugin we can pull down a release artifact and generate our client objects from this.
```
<groupId>org.apache.maven.plugins</groupId>
<artifactId>maven-dependency-plugin</artifactId>
```

### Generating client pojos
For openapi we can use,
```
<groupId>org.openapitools</groupId>
<artifactId>openapi-generator-maven-plugin</artifactId>
```


### Generating json schema from java pojos
Once we have generated our pojo classes from the openapi defintion, its possible to generate the 
equaivalent json schema for the request objects.

For generating client pojos from json schema we can use,
```
<groupId>com.github.victools</groupId>
<artifactId>jsonschema-maven-plugin</artifactId>
```

In this example, the api process publishes its request payload to a kafka topic.
Instead of rewriting a json schema for the schema registry validation, we can take the openapi
pojo objects which are generated, and from that we can generate a json schema which we can use to
upload into the kafka schema registry for the topic payload validation.

## Dependencies
- Depends on demo-api for the api contract 

To run a local version of the schema registry,
Run `tilt up` / `tilt down` to bring up a local kafka schema registry to play with schema loading and validations